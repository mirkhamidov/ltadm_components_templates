<?
$config=array(
  "name" => "Файлы шаблонов",
  "status" => "system",
  "menu_icon" => "icon-pencil",
  "windows" => array(
                      "create" => array("width" => 700,"height" => 550),
                      "edit" => array("width" => 800,"height" => 500),
                      "list" => array("width" => 600,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_templates",
  "list" => array(
    "name" => array("isLink" => true),
    "id_lt_group_templates" => array("disabledSort" => true),
    "filename" => array("align" => "center"),
  ),
  "select" => array(
     "default_orders" => array(
                           array("name" => "ASC"),
                         ),
     "default" => array(
        "id_lt_group_templates" => array(
               "desc" => "Группа шаблонов",
               "type" => "select_from_table",
               "table" => "lt_group_templates",
               "key_field" => "id_lt_group_templates",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "",
               "order" => array ("name" => "ASC"),
               "use_empty" => true,
               "in_list" => true,
             ),


       "id_lt_templates" => array(
           "desc" => "Шаблон в группах",
           "type" => "select_from_tables_as_tree",
           "use_empty" => true,
           "condition" => "id_lt_group_templates>0",
           "links" => array (
                          array(
                            "table" => "lt_group_templates",
                            "key_field" => "id_lt_group_templates",
                            "fields" => array("name"),
                            "show_field" => "%1",
                            "order" => array ("name" => "ASC"),
                          ),
                          array(
                            "table" => "lt_templates",
                            "key_field" => "id_lt_templates",
                            "parent" => "id_lt_group_templates",
                            "fields" => array("name"),
                            "show_field" => "%1",
                            "order" => array ("name" => "ASC"),
                          ),
                       ),
       ),
       "name" => array(
               "desc" => "Шаблоны вне групп",
               "type" => "select_from_table",
               "table" => "lt_templates",
               "key_field" => "name",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "id_lt_group_templates=0",
               "order" => array ("name" => "ASC"),
               "use_empty" => true,
       ),

     ),
  ),

);

$actions=array(
  "create" => array(

    "before_code" => "",

  ),
);

?>