<?
$items=array(
  "id_lt_group_templates" => array(
         "desc" => "Группа шаблона",
         "type" => "select_from_table",
         "table" => "lt_group_templates",
         "key_field" => "id_lt_group_templates",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "use_empty" => true,
         "order" => array ("name" => "ASC"),
         "select_on_edit" => true,
       ),


  "name" => array(
         "desc" => "Название шаблона",
         "type" => "text",
         "maxlength" => "255",
         "size" => "30",
         "select_on_edit" => true,
         "js_not_empty" => true,
         "js_error" => "Поле должно быть не пустым!",
       ),
  "filename" => array(
         "desc" => "Имя файла",
         "type" => "text",
         "maxlength" => "50",
         "size" => "10",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-.]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и '_','-'!",
           ),
         ),
       ),

  "file" => array(
         "desc" => "Содержимое",
         "type" => "text_file_on_disk",
         "file_name_from" => "filename",
         "folder_name_from" => array(
                                 "table" => "lt_group_templates",
                                 "value" => "foldername",
                                 "key_field" => "id_lt_group_templates"
                               ),
         "not_in_table" => true,
         "no_order" => true,
         "no_condition" => true,
         "path" => "../templates/",
         "style" => "width:100%",
         "not_rewrite" => "yes",
         "width" => "70",
         "height" => "20",
         "select_on_edit" => true,
       ),


);
?>